This implementation very simple, If I would have more time, I will be rewrite storing messages to a binary tree. It will be
more scalable implementation and it will reduce the count of allocations of memory.

## PubSub service description

Current pubsub do not have specific place for store all messages. 
We put pointer on message to each subscribers. When player unsubscribe, we remove all messages.
Poll command will return next message for current subscriber (We use pointer, for reduce memory usage).

### Installation

For install, download package:
```
go get bitbucket.org/gollariel/pubsub/queue
```


### Usage

For using package, make storage first `queue.NewStorage()` . After this, you are able to take topic (or create) by it name:
```
s := storage.NewStorage()
tp := s.Topic("topic") // return or create topic
```
Topic provide all API for publish/subscribe/unsubscribe and for poll messages.

For publish message:
```
tp.Publish(bytes)
```

For subscribe:
```
tp.Subscribe(name)
```

For unsubscribe:
```
tp.Unsubscribe(name)
```

For poll next unread message:
```
data, err := tp.Poll()
```
 
### Example usage
 
```go
package main

import (
    "fmt"
    "log"
    
    "bitbucket.org/gollariel/pubsub/queue"
)

func main() {
    s := queue.NewStorage()
    tp := s.Topic("topic")
    tp.Subscribe("your-name")
    tp.Publish([]byte{})
    r, err := tp.Poll("your-name")
    if err != nil {
        log.Fatal(err)
    }
    fmt.Println(r)
}
```

### For developers

For execute tests, run:
```
make test
```

For check test covering:
```
make test-cover
```

For check code with linters:
```
make lint
```

For start working, execute `make init`. This command will add hook with linter command, into git hooks.

_Recommend to use File Watcher with gofmt command._

### Developing story

When I start to implement, I had 3 ideas. But current implementation looks better of other.

First I thought to make map[int][]byte, for store messages, and each subscriber keeps last seen the index, and topic contains the earliest and latest index, and for each Poll, we take the smallest index from subscribers and remove all messages between earliest and minimal index from subscribers. 
This work well, but have a limitation (count of messages), and with the smallest count of allocation, works slowest of the current implementation. 

Another idea, it makes a chain of messages. When each message keeps a link on previous. And topic keeps the link on the first and last message. 
This idea did not implement, because need a lot of relations, and code will be very complex.

