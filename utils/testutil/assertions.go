package testutil

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/pkg/errors"
)

// TestingAssertEqual will check if the two objects are the same
// If not, it will raise an error
func TestingAssertEqual(t *testing.T, actual, expected interface{}) {
	t.Helper()
	switch actual.(type) {
	case error:
		TestingAssertSameError(t, actual.(error), expected.(error))
	case float64:
		TestingAssertFloat(t, actual.(float64), expected.(float64))
	default:
		if !TestingIsEqual(actual, expected) {
			t.Fatalf("mismatched values: %v != %v", actual, expected)
		}
	}
}

// TestingIsEqual compares 2 object is deeply equal or not
func TestingIsEqual(actual, expected interface{}) bool {
	return reflect.DeepEqual(actual, expected)
}

// TestingIsSameError checks if the given errors are either same, or similar
func TestingIsSameError(received, expected error) bool {
	return errors.Cause(received) == errors.Cause(expected)
}

// TestingAssertSameError checks the two errors are same/similar
// If not it will raise an error
func TestingAssertSameError(t *testing.T, received, expected error) {
	t.Helper()
	if !TestingIsSameError(received, expected) {
		t.Fatalf("mismatched errors, received: %+v expected: %+v", received, expected)
	}
}

// TestingAssertFloat asserts the two float values are similar
// It will round the float to 6 decimal places
func TestingAssertFloat(t *testing.T, received, expected float64) {
	t.Helper()
	if !TestingIsSameFloat(received, expected) {
		t.Fatalf("mismatched float, received: %f, expected: %f", received, expected)
	}
}

// TestingIsSameFloat compares the two float values are similar enough
// It will round the float to 6 decimal places
func TestingIsSameFloat(received, expected float64) bool {
	return fmt.Sprintf("%.6f", received) == fmt.Sprintf("%.6f", expected)
}
