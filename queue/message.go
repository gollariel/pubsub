package queue

// Message contains body message
type Message struct {
	Body []byte
}

// NewMessage return new message
func NewMessage(b []byte) *Message {
	return &Message{Body: b}
}
