package queue

import (
	"testing"

	"bitbucket.org/gollariel/pubsub/utils/testutil"
)

type TestSubscriberData struct {
	Topic string
	Name  string
}

type TestPublisherData struct {
	Topic string
	Body  []byte
}

type TestPollData struct {
	Topic      string
	Subscriber string
	Body       []byte
	Err        error
}

func TestUsageFlow(t *testing.T) {
	s := NewStorage()
	tp := s.Topic("test")
	tp.Subscribe("example")
	tp.Subscribe("example2")
	testutil.TestingAssertEqual(t, len(tp.subscribers), 2)
	tp.Publish([]byte{1, 0, 0, 0})
	tp.Publish([]byte{2, 0, 0, 0})
	tp.Unsubscribe("example")
	testutil.TestingAssertEqual(t, len(tp.subscribers), 1)
	d, err := tp.Poll("example")
	testutil.TestingAssertEqual(t, d, []byte{})
	testutil.TestingAssertSameError(t, err, ErrSubscriberNotFound)

	d, err = tp.Poll("example2")
	testutil.TestingAssertEqual(t, d, []byte{1, 0, 0, 0})
	testutil.TestingAssertSameError(t, err, nil)

	d, err = tp.Poll("example2")
	testutil.TestingAssertEqual(t, d, []byte{2, 0, 0, 0})
	testutil.TestingAssertSameError(t, err, nil)

	d, err = tp.Poll("example2")
	testutil.TestingAssertEqual(t, d, []byte{})
	testutil.TestingAssertSameError(t, err, ErrNoNewMessages)
}

func TestStorage(t *testing.T) {
	testcases := map[string]struct {
		publish    []TestPublisherData
		subscribe  []TestSubscriberData
		pollResult []TestPollData
	}{
		"empty_body": {
			publish: []TestPublisherData{
				{
					Topic: "abc123",
					Body:  []byte{},
				},
			},
			subscribe: []TestSubscriberData{
				{
					Topic: "abc123",
					Name:  "example",
				},
			},
			pollResult: []TestPollData{
				{
					Topic:      "abc123",
					Subscriber: "example",
					Body:       []byte{},
				},
			},
		},
		"simple_message": {
			publish: []TestPublisherData{
				{
					Topic: "abc123",
					Body:  []byte{1, 0, 0, 0},
				},
				{
					Topic: "abc123",
					Body:  []byte{2, 0, 0, 0},
				},
			},
			subscribe: []TestSubscriberData{
				{
					Topic: "abc123",
					Name:  "example",
				},
			},
			pollResult: []TestPollData{
				{
					Topic:      "abc123",
					Subscriber: "example",
					Body:       []byte{1, 0, 0, 0},
				},
				{
					Topic:      "abc123",
					Subscriber: "example",
					Body:       []byte{2, 0, 0, 0},
				},
			},
		},
		"no_messages": {
			publish: []TestPublisherData{},
			subscribe: []TestSubscriberData{
				{
					Topic: "abc123",
					Name:  "example",
				},
			},
			pollResult: []TestPollData{
				{
					Topic:      "abc123",
					Subscriber: "example",
					Body:       []byte{},
					Err:        ErrNoNewMessages,
				},
			},
		},
		"wrong_subscriber": {
			publish: []TestPublisherData{
				{
					Topic: "abc123",
					Body:  []byte{1, 0, 0, 0},
				},
			},
			subscribe: []TestSubscriberData{},
			pollResult: []TestPollData{
				{
					Topic:      "abc123",
					Subscriber: "example",
					Body:       []byte{},
					Err:        ErrSubscriberNotFound,
				},
			},
		},
	}

	for name, test := range testcases {
		t.Run(name, func(t *testing.T) {
			s := NewStorage()
			for _, v := range test.subscribe {
				tp := s.Topic(v.Topic)
				tp.Subscribe(v.Name)
			}

			for _, v := range test.publish {
				tp := s.Topic(v.Topic)
				tp.Publish(v.Body)
			}

			for _, v := range test.pollResult {
				tp := s.Topic(v.Topic)
				body, err := tp.Poll(v.Subscriber)

				testutil.TestingAssertSameError(t, v.Err, err)
				testutil.TestingAssertEqual(t, v.Body, body)
			}
		})
	}
}

/*
BenchmarkPubSub-4                        2000000               622 ns/op             636 B/op         10 allocs/op
BenchmarkPubSub100Publish-4               100000             18968 ns/op            6232 B/op        215 allocs/op
BenchmarkPubSub100PublishAndPoll-4        100000             20769 ns/op            4992 B/op        307 allocs/op
*/

func BenchmarkPubSub(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s := NewStorage()
		tp := s.Topic("test")
		tp.Subscribe("example")
		tp.Publish([]byte{5, 0, 0, 0})
		_, err := tp.Poll("example")
		if err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkPubSub100Publish(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s := NewStorage()
		tp := s.Topic("test")
		tp.Subscribe("example")
		for i := 0; i < 100; i++ {
			tp.Publish([]byte{5, 0, 0, 0})
		}
		for i := 0; i < 100; i++ {
			_, err := tp.Poll("example")
			if err != nil {
				b.Fatal(err)
			}
		}
	}
}

func BenchmarkPubSub100PublishAndPoll(b *testing.B) {
	for i := 0; i < b.N; i++ {
		s := NewStorage()
		tp := s.Topic("test")
		tp.Subscribe("example")
		for i := 0; i < 100; i++ {
			tp.Publish([]byte{5, 0, 0, 0})
			_, err := tp.Poll("example")
			if err != nil {
				b.Fatal(err)
			}
		}
	}
}
