package queue

import (
	"testing"

	"bitbucket.org/gollariel/pubsub/utils/testutil"
)

type PollsTest struct {
	Data []byte
	Err  error
}

func TestSubscriber(t *testing.T) {
	testcases := map[string]struct {
		messages []*Message
		polls    []*PollsTest
	}{
		"no_message": {
			messages: nil,
			polls:    []*PollsTest{},
		},
		"message": {
			messages: []*Message{
				NewMessage([]byte{1, 0, 0, 0}),
				NewMessage([]byte{2, 0, 0, 0}),
				NewMessage([]byte{3, 0, 0, 0}),
			},
			polls: []*PollsTest{{
				Data: []byte{1, 0, 0, 0},
				Err:  nil,
			}, {
				Data: []byte{2, 0, 0, 0},
				Err:  nil,
			}, {
				Data: []byte{3, 0, 0, 0},
				Err:  nil,
			}},
		},
	}
	for name, test := range testcases {
		t.Run(name, func(t *testing.T) {
			s := NewSubscriber()
			if test.messages != nil {
				for _, m := range test.messages {
					s.Push(m)
				}
			}

			for _, p := range test.polls {
				v, err := s.Poll()
				testutil.TestingAssertEqual(t, p.Err, err)
				testutil.TestingAssertEqual(t, p.Data, v)
			}
		})
	}
}
