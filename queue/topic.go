package queue

import (
	"sync"
)

// Topic contains all subscribers and not read messages
type Topic struct {
	subscribers map[string]*Subscriber
	mu          sync.RWMutex
}

// NewTopic return new topic
func NewTopic() *Topic {
	return &Topic{
		subscribers: map[string]*Subscriber{},
	}
}

// Subscribe subscribe by name for a new messages
func (t *Topic) Subscribe(name string) {
	t.mu.Lock()
	t.subscribers[name] = NewSubscriber()
	t.mu.Unlock()
}

// Unsubscribe unsubscribe from topic
func (t *Topic) Unsubscribe(name string) {
	t.mu.Lock()
	delete(t.subscribers, name)
	t.mu.Unlock()
}

// Poll take message for a subscriber by name
func (t *Topic) Poll(name string) ([]byte, error) {
	t.mu.RLock()
	s, ok := t.subscribers[name]
	if !ok {
		t.mu.RUnlock()
		return []byte{}, ErrSubscriberNotFound
	}
	v, err := s.Poll()
	t.mu.RUnlock()
	return v, err
}

// Publish put message to queue
func (t *Topic) Publish(data []byte) {
	t.mu.Lock()
	m := NewMessage(data)
	for _, s := range t.subscribers {
		s.Push(m)
	}
	t.mu.Unlock()
}
