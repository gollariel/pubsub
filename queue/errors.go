package queue

import "errors"

// All kind of error for this package
var (
	ErrSubscriberNotFound = errors.New("subscriber not found")
	ErrNoNewMessages      = errors.New("no new messages")
)
