package queue

import "sync"

// Storage base storage for all topics and messages
type Storage struct {
	topics map[string]*Topic
	mu     sync.Mutex
}

// NewStorage return new storage
func NewStorage() *Storage {
	return &Storage{
		topics: map[string]*Topic{},
	}
}

// Topic get topic or create new one
func (s *Storage) Topic(name string) *Topic {
	s.mu.Lock()
	v, ok := s.topics[name]
	if !ok {
		v = NewTopic()
		s.topics[name] = v
	}
	s.mu.Unlock()
	return v
}
