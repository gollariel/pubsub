package queue

import (
	"sync"
)

// Subscriber contains all pointers on message, what must be delivered
type Subscriber struct {
	messages []*Message
	mu       sync.Mutex
}

// NewSubscriber return new subscriber
func NewSubscriber() *Subscriber {
	return &Subscriber{}
}

// Push add message
func (s *Subscriber) Push(m *Message) {
	s.mu.Lock()
	s.messages = append(s.messages, m)
	s.mu.Unlock()
}

// Take message
func (s *Subscriber) Poll() ([]byte, error) {
	s.mu.Lock()
	if len(s.messages) == 0 {
		s.mu.Unlock()
		return []byte{}, ErrNoNewMessages
	}
	var m *Message
	m, s.messages = s.messages[0], s.messages[1:]
	s.mu.Unlock()
	return m.Body, nil
}
