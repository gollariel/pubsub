ALL: format build

test:
	go test -timeout 10s -race -p 1 ./...

test-cover:
	go test -cover -p 1 ./...

bench:
	go test -cover -p 1  -bench=. -benchmem ./...

format:
	gofmt -s -w **/*.go

init:
	chmod -R +x .githooks/
	mkdir -p .git/hooks/
	find .git/hooks -type l -exec rm {} \;
	find .githooks -type f -exec ln -sf ../../{} .git/hooks/ \;

dep:
	GO111MODULE=off go get -u github.com/golangci/golangci-lint/cmd/golangci-lint

lint: dep
	golangci-lint run --skip-dirs=vendor --disable-all --enable=goimports --enable=gosimple --enable=typecheck --enable=unused --enable=golint --enable=deadcode --enable=structcheck --enable=varcheck --enable=errcheck --enable=ineffassign --enable=govet --enable=staticcheck  --deadline=3m ./...
